function containsChar(str, charToFind) {
  if (!str || !charToFind) {
    return false;
  }
  return str.indexOf(charToFind) !== -1;
}

function countChar(str, charToCount) {
  if (!str || !charToCount) {
    return NaN;
  }
  let count = 0;
  let index = str.indexOf(charToCount);
  while (index !== -1) {
    count++;
    index = str.indexOf(charToCount, index + 1);
  }
  return count;
}

function getChars(str) {
  if (!str) {
    return [];
  }
  let chars = [];
  for (let i = 0; i < str.length; i++) {
    const actChar = str[i];
    if (chars.indexOf(actChar) === -1) {
      chars.push(actChar);
    }
  }
  return chars;
}

function getMostFrequentChar(str) {
  if (!str) {
    return {};
  }
  // erster Character zählen
  let actChar = str[0];
  let actFrequency = countChar(str, actChar);
  let mostFrequentChar = {
    char: actChar,
    frequency: actFrequency,
  };
  // weitere Character zählen, wobei nicht überprüft
  // wird ob Character schon einmal gezählt wurde
  for (let i = 1; i < str.length; i++) {
    actChar = str[i];
    actFrequency = countChar(str, actChar);
    if (mostFrequentChar.frequency < actFrequency) {
      mostFrequentChar.char = actChar;
      mostFrequentChar.frequency = actFrequency;
    }
  }
  return mostFrequentChar;
}

export { containsChar, countChar, getChars, getMostFrequentChar };
