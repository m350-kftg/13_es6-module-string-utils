import {
  countChar,
  containsChar,
  getChars,
  getMostFrequentChar,
} from '../scripts/stringUtil';

describe('how many times a string contains a character', () => {
  test('a is 5 times in abaabaa', () => {
    // given - nothing to setup
    // when
    const nrOfChar = countChar('abaabaa', 'a');
    // then
    expect(nrOfChar).toBe(5);
  });

  test('a is not a single time in hello', () => {
    // given - nothing to setup
    // when
    const nrOfChar = countChar('hello', 'a');
    // then
    expect(nrOfChar).toBe(0);
  });

  test('not possible to count character in null string', () => {
    // given - nothing to setup
    // when
    const nrOfChar = countChar(null, 'a');
    // then
    expect(isNaN(nrOfChar)).toBeTruthy();
  });

  test('not possible to count character in empty string', () => {
    // given - nothing to setup
    // when
    const nrOfChar = countChar('', 'a');
    // then
    expect(isNaN(nrOfChar)).toBeTruthy();
  });

  test('not possible to count null character in string', () => {
    // given - nothing to setup
    // when
    const nrOfChar = countChar('hello', null);
    // then
    expect(isNaN(nrOfChar)).toBeTruthy();
  });

  test('not possible to count empty character in string', () => {
    // given - nothing to setup
    // when
    const nrOfChar = countChar('hello', '');
    // then
    expect(isNaN(nrOfChar)).toBeTruthy();
  });
});

describe('tests if a character is part of a string', () => {
  test('e is part of hello', () => {
    // given - nothing to setup
    // when
    const doesContain = containsChar('hello', 'e');
    // then
    expect(doesContain).toBeTruthy();
  });

  test('a is not part of hello', () => {
    // given - nothing to setup
    // when
    const doesContain = containsChar('hello', 'a');
    // then
    expect(doesContain).not.toBeTruthy();
    // or
    expect(doesContain).toBeFalsy();
  });

  test('not possible to find character in null string', () => {
    // given - nothing to setup
    // when
    const doesContain = containsChar(null, 'a');
    // then
    expect(doesContain).toBeFalsy();
  });

  test('not possible to find character in empty string', () => {
    // given - nothing to setup
    // when
    const doesContain = containsChar('', 'a');
    // then
    expect(doesContain).toBeFalsy();
  });

  test('not possible to find null character in string', () => {
    // given - nothing to setup
    // when
    const doesContain = containsChar('hello', null);
    // then
    expect(doesContain).toBeFalsy();
  });

  test('not possible to find empty char in string', () => {
    // given - nothing to setup
    // when
    const doesContain = containsChar('hello', '');
    // then
    expect(doesContain).toBeFalsy();
  });
});

describe('getting a list of all characters in a string', () => {
  test('xyzzxyz contains x, y and z', () => {
    // given - nothing to setup
    // when
    const chars = getChars('xyzzxyz');
    // then
    expect(chars).toEqual(['x', 'y', 'z']);
  });

  test('no character in null string', () => {
    // given - nothing to setup
    // when
    const chars = getChars(null);
    // then
    expect(chars).toEqual([]);
  });

  test('no character in empty string', () => {
    // given - nothing to setup
    // when
    const chars = getChars('');
    // then
    expect(chars).toEqual([]);
  });
});

describe('getting most frequent character and its frequency in a string', () => {
  test('o is the most frequent (5 times) in lolooxloo', () => {
    // given - nothing to setup
    // when
    const charFreqency = getMostFrequentChar('lolooxloo');
    // then
    expect(charFreqency).toEqual({ char: 'o', frequency: 5 });
  });

  test('first character is taken if other characters have same frequency', () => {
    // given - nothing to setup
    // when
    const charFreqency = getMostFrequentChar('cddeeccde');
    // then
    expect(charFreqency).toEqual({ char: 'c', frequency: 3 });
  });

  test('no character and frequency in null string', () => {
    // given - nothing to setup
    // when
    const charFreqency = getMostFrequentChar(null);
    // then
    expect(charFreqency).toEqual({});
  });

  test('no character and frequency in empty string', () => {
    // given - nothing to setup
    // when
    const charFreqency = getMostFrequentChar('');
    // then
    expect(charFreqency).toEqual({});
  });
});
